#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dlEvilInc.py
"""

__author__ = 'Christopher W. Carter'
__email__ = 'Ricroar.Cypher@gmail.com'
__license__ = "Copyright 2014 : All Rights Reserved"


import argparse
import logging
import sys

import urllib.request

from bs4 import BeautifulSoup

class dlEvilInc(object):
	"""
	dlEvilInc
	"""
	def __init__(self):
		self.url_archive_page = r'http://evil-inc.com/archive/'
		self.url_comic_page = r'http://evil-inc.com/comic/monday-3/'
		self.url_comic_image = r'http://evil-inc.com/wp-content/uploads/2013/06/2005-05-30-Monday1.jpg'
		self.url_base = r'http://evil-inc.com'
		

	#Properties:  ########################################################

	#Functions:  #########################################################
	def get_image_url(self, page):
		return page.find_all(property="og:image")[0].get('content')
	
	#Decorators:  ########################################################

	#Run Function:  ######################################################
	def run(self):
		"""Basic functionality of script."""
		#~ url = self.url_archive + r'?p=1'
		url = self.url_archive_page
		soup = BeautifulSoup(urllib.request.urlopen(url))
		#~ print(soup.prettify())
		#~ print()

		with open('./EvilInc_links.txt', 'a+') as f:
			for link in soup.find_all(attrs={"class": "comic-archive-title"}):
				print(link.find('a').get('href'))
				page = BeautifulSoup(urllib.request.urlopen(link.find('a').get('href')))
				link_comic = self.get_image_url(page)
				print(link_comic)
				f.write(link_comic + '\n')
				
		with open('./GirlGenius_links.txt', 'a+') as f:
			link_next = self.url_archive_page
			while(link_next is not None):
				print(link_next)
				req = urllib.request.Request(link_next, None, self.headers)
				soup = BeautifulSoup(urllib.request.urlopen(req))
				link_comic = self.get_image_url(soup)
				print(link_comic)
				f.write(link_comic + '\n')
				link_next = self.get_next_page(soup)

# Parse Arguments:  ####################################################
def parseArgs(args):
    """ Parse the command-line arguments. """

    parser = argparse.ArgumentParser(
        description='Gets the image url of the comic EvilInc.'
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Print debug information',
        action='store_true', default=False
    )
    return parser.parse_args(args)

# Main:  ###############################################################
def main():
	"""Parses arguments, initializes class, and returns class.run()."""

	args = parseArgs(sys.argv[1:])

	if args.verbose:
		logging.basicConfig(
			format='%(levelname)s %(filename)s: %(message)s',
			level=logging.DEBUG
		)
	else:
		# Log info and above to console
		logging.basicConfig(
			format='%(levelname)s: %(message)s',
			level=logging.INFO
		)

	mod = dlEvilInc()
	mod.run()

if __name__ == '__main__':
	main()
