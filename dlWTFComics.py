#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dlWTFComics.py
"""

__author__ = 'Christopher W. Carter'
__email__ = 'Ricroar.Cypher@gmail.com'
__license__ = "Copyright 2014 : All Rights Reserved"


import argparse
import logging
import sys

import urllib.request

from bs4 import BeautifulSoup

class dlWTFComics(object):
	"""
	dlWTFComics
	"""
	def __init__(self):
		self.url_archive_page = r'http://wtfcomics.com/archive.html?439_439'
		self.url_comic_image = r'http://www.s214135040.onlinehome.us/strip438.jpg'
		self.url_base = r'http://wtfcomics.com'
		
		self.user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0'
		self.headers = { 'User-Agent' : self.user_agent }

	#Properties:  ########################################################

	#Functions:  #########################################################
	def get_image_url(self, strip_number):
		return r'http://www.s214135040.onlinehome.us/strip{0}.jpg'.format(strip_number)
	
	#Decorators:  ########################################################

	#Run Function:  ######################################################
	def run(self):
		"""Basic functionality of script."""
		#~ url = self.url_archive_page
		#~ soup = BeautifulSoup(urllib.request.urlopen(url))
		#~ print(soup.prettify())
		#~ print()
		with open('./WTFComics_links.txt', 'a+') as f:
			for strip_number in range(1, 439, 1):
				link_comic = self.get_image_url(strip_number)
				print(link_comic)
				f.write(link_comic + '\n')

# Parse Arguments:  ####################################################
def parseArgs(args):
    """ Parse the command-line arguments. """

    parser = argparse.ArgumentParser(
        description='Gets the image url of the comic WTFComics.'
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Print debug information',
        action='store_true', default=False
    )
    return parser.parse_args(args)

# Main:  ###############################################################
def main():
	"""Parses arguments, initializes class, and returns class.run()."""

	args = parseArgs(sys.argv[1:])

	if args.verbose:
		logging.basicConfig(
			format='%(levelname)s %(filename)s: %(message)s',
			level=logging.DEBUG
		)
	else:
		# Log info and above to console
		logging.basicConfig(
			format='%(levelname)s: %(message)s',
			level=logging.INFO
		)

	mod = dlWTFComics()
	mod.run()

if __name__ == '__main__':
	main()
