#!/bin/bash

startYear=03
startMonth=10
currentYear=$(date +%y)
currentMonth=$(date +$(date +%y))

# Leading Year (no point in downloading anything before first comic)
for year in $(seq --equal-width $startYear $startYear);
	do for month in $(seq --equal-width $startMonth 12);
		do for day in $(seq --equal-width 01 31);
			do wget --progress=bar --random-wait --mirror --convert-links --no-parent --page-requisites --no-host-directories --no-directories "http://cdn.twokinds.keenspot.com/comics/20$year$month$day.jpg";
		done;
	done;
done

# Inbetween Years (try to download everything for any given day)
for year in $(seq --equal-width $(expr $startYear + 1) $(expr $currentYear - 1));
	do for month in $(seq --equal-width 01 12);
		do for day in $(seq --equal-width 01 31);
			do wget --progress=bar --random-wait --mirror --convert-links --no-parent --page-requisites --no-host-directories --no-directories "http://cdn.twokinds.keenspot.com/comics/20$year$month$day.jpg";
		done;
	done;
done

# Current Year (no point in trying to download anything after current month)
for year in $(seq --equal-width $currentYear $currentYear);
	do for month in $(seq --equal-width 01 $currentMonth);
		do for day in $(seq --equal-width 01 31);
			do wget --progress=bar --random-wait --mirror --convert-links --no-parent --page-requisites --no-host-directories --no-directories "http://cdn.twokinds.keenspot.com/comics/20$year$month$day.jpg";
		done;
	done;
done
