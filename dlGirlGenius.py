#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dlTwoKinds.py
"""

__author__ = 'Christopher W. Carter'
__email__ = 'Ricroar.Cypher@gmail.com'
__license__ = "Copyright 2014 : All Rights Reserved"


import argparse
import logging
import sys

import urllib.request

from bs4 import BeautifulSoup

class dlTwoKinds(object):
	"""
	dlTwoKinds
	"""
	def __init__(self):
		self.url_archive_page = r'http://www.girlgeniusonline.com/comic.php?date=20021104'
		self.url_base = r'http://www.girlgeniusonline.com/'
		
		self.user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0'
		self.headers = { 'User-Agent' : self.user_agent }

	#Properties:  ########################################################

	#Functions:  #########################################################
	def get_image_url(self, soup):
		#~ <IMG ALT='Comic' BORDER=0 SRC='http://www.girlgeniusonline.com/ggmain/strips/ggmain20140929.jpg' WIDTH='700' HEIGHT='1039'/>
		return soup.find(alt="Comic").get('src')
	def get_next_page(self, soup):
		#~ <a href="http://www.girlgeniusonline.com/comic.php?date=20021106" id="topnext" title="The Next Comic"></a>
		try:
			link_next = soup.find(id="topnext").get('href')
		except AttributeError as err:
			link_next = None
		return link_next
	
	#Decorators:  ########################################################

	#Run Function:  ######################################################
	def run(self):
		"""Basic functionality of script."""
		#~ url = self.url_archive_page
		#~ soup = BeautifulSoup(urllib.request.urlopen(url))
		#~ print(soup.prettify())
		#~ print()
		with open('./GirlGenius_links.txt', 'a+') as f:
			link_next = self.url_archive_page
			while(link_next is not None):
				print(link_next)
				req = urllib.request.Request(link_next, None, self.headers)
				soup = BeautifulSoup(urllib.request.urlopen(req))
				link_comic = self.get_image_url(soup)
				print(link_comic)
				f.write(link_comic + '\n')
				link_next = self.get_next_page(soup)

# Parse Arguments:  ####################################################
def parseArgs(args):
    """ Parse the command-line arguments. """

    parser = argparse.ArgumentParser(
        description='Gets the image url of the comic Twokinds.'
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Print debug information',
        action='store_true', default=False
    )
    return parser.parse_args(args)

# Main:  ###############################################################
def main():
	"""Parses arguments, initializes class, and returns class.run()."""

	args = parseArgs(sys.argv[1:])

	if args.verbose:
		logging.basicConfig(
			format='%(levelname)s %(filename)s: %(message)s',
			level=logging.DEBUG
		)
	else:
		# Log info and above to console
		logging.basicConfig(
			format='%(levelname)s: %(message)s',
			level=logging.INFO
		)

	mod = dlTwoKinds()
	mod.run()

if __name__ == '__main__':
	main()
