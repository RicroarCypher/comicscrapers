#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dlTwoKinds.py
"""

__author__ = 'Christopher W. Carter'
__email__ = 'Ricroar.Cypher@gmail.com'
__license__ = "Copyright 2014 : All Rights Reserved"


import argparse
import logging
import sys

import urllib.request

from bs4 import BeautifulSoup

class dlTwoKinds(object):
	"""
	dlTwoKinds
	"""
	def __init__(self):
		self.url_archive_page = r'http://twokinds.keenspot.com/?p=archive'
		self.url_archive = r'http://twokinds.keenspot.com/archive.php'
		self.url_base = r'http://twokinds.keenspot.com/'
		

	#Properties:  ########################################################

	#Functions:  #########################################################
	def get_image_url(self, page):
		return page.find(id="cg_img").find('img').get('src')
	
	#Decorators:  ########################################################

	#Run Function:  ######################################################
	def run(self):
		"""Basic functionality of script."""
		#~ url = self.url_archive + r'?p=1'
		url = self.url_archive_page
		soup = BeautifulSoup(urllib.request.urlopen(url))
		#~ print(soup.prettify())
		#~ print()

		for link in soup.find_all(attrs={"class": "chapter"}):
			for att in link.find_all('a'):
				page_url = self.url_base + att.get('href')
				page = BeautifulSoup(urllib.request.urlopen(page_url))
				print(self.get_image_url(page))

# Parse Arguments:  ####################################################
def parseArgs(args):
    """ Parse the command-line arguments. """

    parser = argparse.ArgumentParser(
        description='Gets the image url of the comic Twokinds.'
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Print debug information',
        action='store_true', default=False
    )
    return parser.parse_args(args)

# Main:  ###############################################################
def main():
	"""Parses arguments, initializes class, and returns class.run()."""

	args = parseArgs(sys.argv[1:])

	if args.verbose:
		logging.basicConfig(
			format='%(levelname)s %(filename)s: %(message)s',
			level=logging.DEBUG
		)
	else:
		# Log info and above to console
		logging.basicConfig(
			format='%(levelname)s: %(message)s',
			level=logging.INFO
		)

	mod = dlTwoKinds()
	mod.run()

if __name__ == '__main__':
	main()
